﻿using AutoMapper;
using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using RushHour.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RushHour.Web.Controllers
{
    public class BaseController<TEntity, TViewModel> : Controller
        where TEntity : BaseModel, new()
        where TViewModel : BaseViewModel, new()
    {
        private IService<TEntity> _service;

        private BaseIndexViewModel indexViewModel;
        private BaseCreateViewModel createViewModel;
        private BaseEditViewModel editViewModel;
        private BaseDeleteViewModel deleteViewModel;
        private IUnitOfWork unitOfWork;

        public BaseController()
        {
            unitOfWork = new UnitOfWork();
            _service = new DataService<TEntity>(unitOfWork);
        }

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        /*public JsonResult LoadData()
        {
            IEnumerable<TEntity> entities = _service.GetAll();
            List<TViewModel> entityViewModels = new List<TViewModel>();

            foreach (var entity in entities)
            {
                TViewModel entityViewModel = new TViewModel();
                Mapper.Map(entity, entityViewModel);
                entityViewModels.Add(entityViewModel);
            }

            return Json(new { data = entityViewModels }, JsonRequestBehavior.AllowGet);
        }
        */

        public ActionResult Details(int id)
        {
            TEntity entity = _service.Get(id);
            return View(entity);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(TViewModel entityViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(entityViewModel);
            }

            TEntity entity = new TEntity();
            Mapper.Map(entityViewModel, entity);

            _service.Insert(entity);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            TEntity entity = _service.Get(id);
            if (entity == null)
            {
                return HttpNotFound();
            }

            TViewModel entityEditViewModel = new TViewModel();
            Mapper.Map(entity, entityEditViewModel);

            return View(entityEditViewModel);
        }

        [HttpPost]
        public ActionResult Edit(TViewModel entityViewModel)
        { 
            if (!ModelState.IsValid)
            {
                return View(entityViewModel);
            }

            TEntity entity = new TEntity();
            Mapper.Map(entityViewModel, entity);
            _service.Update(entity);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            TEntity entity = _service.Get(id);

            if (entity == null)
            {
                return HttpNotFound();
            }

            TViewModel entityDeleteViewModel = new TViewModel
            {
                Id = entity.Id
            };

            return View(entityDeleteViewModel);
        }

        [HttpPost]
        public ActionResult Delete(TEntity entity)
        {
            _service.Delete(entity);

            return RedirectToAction("Index");
        }
    }
}