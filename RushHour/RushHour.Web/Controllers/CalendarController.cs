﻿using AutoMapper;
using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using RushHour.Web.ViewModels.AppointmentViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RushHour.Web.Controllers
{
    public class CalendarController : Controller
    {
        private IUnitOfWork unitOfWork;
        private IService<Appointment> appointmentService;

        public CalendarController()
        {
            unitOfWork = new UnitOfWork();
            appointmentService = new AppointmentService(unitOfWork);

        }
        // GET: Calendar
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveAppointment(AppointmentCreateViewModel appointmentViewModel)
        {
            var status = false;

            Appointment appointment = new Appointment();
            Mapper.Map(appointmentViewModel, appointment);
            if (appointmentViewModel.Id > 0)
            {
                appointmentService.Update(appointment);
                status = true;
            }
            else
            {
                appointmentService.Insert(appointment);
                status = true;
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public JsonResult DeleteAppointment(int eventID)
        {
            var status = false;
            Appointment appointment = appointmentService.Get(eventID);
            if (appointment!=null)
            {
                appointmentService.Delete(appointment);
                status = true;
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}