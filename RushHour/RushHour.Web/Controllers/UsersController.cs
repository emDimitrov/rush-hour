﻿using AutoMapper;
using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using RushHour.Web.Authentication;
using RushHour.Web.ViewModels.UserViewModel;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RushHour.Web.Controllers
{
    public class UsersController : BaseController<User, UserBaseViewModel>
    {
        private IService<User> _service;

        private IUnitOfWork unitOfWork;

        public UsersController()
        {
            unitOfWork = new UnitOfWork();
            _service = new UserService(unitOfWork);
        }

        public JsonResult LoadData()
        {
            IEnumerable<User> users = _service.GetAll();
            List<UserBaseViewModel> userViewModels = new List<UserBaseViewModel>();

            foreach (var user in users)
            {
                UserBaseViewModel userViewModel = new UserBaseViewModel();
                Mapper.Map(user, userViewModel);
                userViewModels.Add(userViewModel);
            }

            return Json(new {data = userViewModels }, JsonRequestBehavior.AllowGet);
        }

        /*
        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }
        // GET: Users

        public ActionResult Details(int id)
        {
            User user = _service.Get(id);
            return View(user);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(UserCreateViewModel userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(userViewModel);
            }

            User user = new User();
            Mapper.Map(userViewModel, user);

            _service.Insert(user);

            return RedirectToAction("Index");
        }

        [CustomAuthorize]
        public ActionResult Edit(int id)
        {
            User user = _service.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            UserEditViewModel userViewModel = new UserEditViewModel();
            Mapper.Map(user, userViewModel);

            return View(userViewModel);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Edit(UserEditViewModel userViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(userViewModel);
            }

            User user = new User();
            Mapper.Map(userViewModel, user);
            this._service.Update(user);

            return RedirectToAction("Index");
        }

        [CustomAuthorize]
        public ActionResult Delete(int id)
        {
            User user =_service.Get(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            UserDeleteViewModel userViewModel = new UserDeleteViewModel();
            Mapper.Map(user, userViewModel);

            return View(userViewModel);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Delete(User user)
        {
            _service.Delete(user);

            return RedirectToAction("Index");
        }
        */
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel userViewModel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Error = "Incorrect Email or Password";
                return View(userViewModel);
            }

            AuthenticationManager.Authenticate(userViewModel.Email, userViewModel.Password);
            if (AuthenticationManager.LoggedUser == null)
            {
                System.Web.HttpContext.Current.Session["LoggedUser"] = null;
                return View("Login");
            }

            Session["Email"] = userViewModel.Email;

            return RedirectToAction("Index", "Appointments");
        }

        public ActionResult Logout()
        {
            AuthenticationManager.LogOut();
            return RedirectToAction("Index", "Activities");
        }
    }
}