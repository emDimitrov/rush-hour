﻿using RushHour.Model;
using RushHour.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RushHour.Web.ViewModels.AppointmentViewModel;
using RushHour.Data;
using RushHour.Web.CustomAttributes;
using RushHour.Web.Authentication;

namespace RushHour.Web.Controllers
{
    public class AppointmentsController : Controller
    {
        private IService<Appointment> _service;
        private IService<User> _userService;
        private IService<Activity> _activityService;
        private IUnitOfWork unitOfWork;

        public AppointmentsController()
        {
            unitOfWork = new UnitOfWork();
            _service = new AppointmentService(unitOfWork);
            _activityService = new DataService<Activity>(unitOfWork);
            _userService = new DataService<User>(unitOfWork);
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult Calendar()
        {
            return View();
        }

        public JsonResult LoadData()
        {
            User loggedUser = AuthenticationManager.LoggedUser;
            IEnumerable<Appointment> appointments = _service.GetAll(a => a.UserId == loggedUser.Id).ToList();
            IEnumerable<Activity> activities = _activityService.GetAll().ToList();

            List<AppointmentIndexViewModel> appointmentsToDisplay = new List<AppointmentIndexViewModel>();
            AppointmentIndexViewModel appointmentViewModel;
            foreach (var item in appointments)
            {
                appointmentViewModel = new AppointmentIndexViewModel
                {
                    ActivityNames = new List<string>(),
                    Id = item.Id,
                    StartDateTime = item.StartDateTime.ToString(),
                    EndDateTime = item.EndDateTime.ToString(),
                    UserId = item.UserId
                };

                for (int i = 0; i < item.Activities.Count; i++)
                {
                    ActivityCheckBox activityCheckBox = new ActivityCheckBox()
                    {
                        CheckedActivity = item.Activities.ToList()[i],
                        Id = item.Activities.ToList()[i].Id
                    };

                    appointmentViewModel.ActivityNames.Add(activityCheckBox.CheckedActivity.Name);
                }

                appointmentsToDisplay.Add(appointmentViewModel);
            }

                return new JsonResult { Data = appointmentsToDisplay, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Details(int id)
        {
            Appointment appointment = _service.Get(id);
            return View(appointment);
        }

        [CustomAuthorize]
        public ActionResult Create()
        {
            ICollection<Activity> activities = _activityService.GetAll().ToList();
            List<ActivityCheckBox> checkBoxes = new List<ActivityCheckBox>();
            foreach (var item in activities)
            {
                checkBoxes.Add(
                        new ActivityCheckBox
                        {
                            CheckedActivity = item,
                            IsChecked = false,
                            Id = item.Id
                        }
                    );
            }

            AppointmentCreateViewModel appointmentViewModel = new AppointmentCreateViewModel
            {
                Activities = checkBoxes,
                StartDateTime = DateTime.Now,
                UserId = 1
            };

            return View(appointmentViewModel);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Create(AppointmentCreateViewModel appointmentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(appointmentViewModel);
            }

            List<Activity> checkedActivities = new List<Activity>();
            foreach (var item in appointmentViewModel.Activities)
            {
                if (item.IsChecked==true)
                {
                    checkedActivities.Add(_activityService.Get(item.Id));
                }
            }

            int userId = AuthenticationManager.LoggedUser.Id;
            Appointment appointment = new Appointment
            {
                StartDateTime = appointmentViewModel.StartDateTime,
                EndDateTime = appointmentViewModel.StartDateTime,
                UserId = userId,
                Activities = checkedActivities
            };

            _service.Insert(appointment);
            return RedirectToAction("Index");
        }

        [CustomAuthorize]
        public ActionResult Edit(int id)
        {
            Appointment appointment = _service.Get(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }

            
            List<Activity> checkedActivities = appointment.Activities.ToList();
            ICollection<Activity> allActivities = _activityService.GetAll().ToList();
            List<ActivityCheckBox> checkBoxes = new List<ActivityCheckBox>();
            foreach (var activity in allActivities)
            {
                bool isChecked = false;
                if (checkedActivities.IndexOf(activity) >= 0)
                {
                    isChecked = true;
                }
                checkBoxes.Add(
                        new ActivityCheckBox
                        {
                            Id = activity.Id,
                            CheckedActivity = activity,
                            IsChecked = isChecked
                        }
                    );
            }

            AppointmentEditViewModel appointmentViewModel = new AppointmentEditViewModel
            {
                StartDateTime = appointment.StartDateTime,
                Activities = checkBoxes,
                UserId = appointment.UserId,
                Id = appointment.Id
            };

            return View(appointmentViewModel);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Edit(AppointmentEditViewModel appointmentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(appointmentViewModel);
            }

            Appointment appointment = _service.Get(appointmentViewModel.Id);

            for (int i = 0; i < appointmentViewModel.Activities.Count(); i++)
            {
                Activity activity = _activityService.Get(appointmentViewModel.Activities[i].Id);
                if (appointmentViewModel.Activities[i].IsChecked==true)
                {
                   appointment.Activities.Add(activity);
                }
                else
                {
                    appointment.Activities.Remove(activity);
                }
            }

            appointment.StartDateTime = appointmentViewModel.StartDateTime;
            _service.Update(appointment);

            return RedirectToAction("Index");
        }

        [CustomAuthorize]
        public ActionResult Delete(int id)
        {
            Appointment appointment = _service.Get(id);

            if (appointment == null)
            {
                return HttpNotFound();
            }

            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Delete(AppointmentDeleteViewModel appointmentViewModel)
        {
            Appointment appointment = _service.Get(appointmentViewModel.Id);
            _service.Delete(appointment);
            return RedirectToAction("Index");
        }
    }
}