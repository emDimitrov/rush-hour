﻿using RushHour.Service;
using System;
using System.Collections.Generic;
using RushHour.Data.Repositories;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RushHour.Model;
using RushHour.Web.ViewModels.ActivityViewModel;
using RushHour.Data;
using System.Globalization;
using AutoMapper;
using RushHour.Web.ViewModels;

namespace RushHour.Web.Controllers
{
    public class ActivitiesController : BaseController<Activity, ActivityBaseViewModel>
    {
        private IService<Activity> _service;
        
        private IUnitOfWork unitOfWork;

        public ActivitiesController()
        {
            unitOfWork = new UnitOfWork();
            _service = new DataService<Activity>(unitOfWork);
        }

        public JsonResult LoadData()
        {
            IEnumerable<Activity> activities = _service.GetAll();
            List<ActivityBaseViewModel> activityViewModels = new List<ActivityBaseViewModel>();

            foreach (var activity in activities)
            {
                ActivityBaseViewModel activityViewModel = new ActivityBaseViewModel();
                Mapper.Map(activity, activityViewModel);
                activityViewModels.Add(activityViewModel);
            }

            return Json(new { data = activityViewModels }, JsonRequestBehavior.AllowGet);
        }

        /*public ActionResult Details(int id)
        {
            Activity activity = _service.Get(id);
            return View(activity);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ActivityCreateViewModel activityViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(activityViewModel);
            }

            Activity activity = new Activity();
            Mapper.Map(activityViewModel, activity);

            _service.Insert(activity);
 
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Activity activity = _service.Get(id);
            if (activity == null)
            {
                return HttpNotFound();
            }

            ActivityEditViewModel activityEditViewModel = new ActivityEditViewModel();
            Mapper.Map(activity, activityEditViewModel);

            return View(activityEditViewModel);
        }

        [HttpPost]
        public ActionResult Edit(ActivityEditViewModel activityViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(activityViewModel);
            }

            Activity activity = new Activity();
            Mapper.Map(activityViewModel, activity);
            _service.Update(activity);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Activity activity = _service.Get(id);

            if (activity == null)
            {
                return HttpNotFound();
            }

            ActivityDeleteViewModel activityDeleteViewModel = new ActivityDeleteViewModel
            { 
                Name = activity.Name
            };

            return View(activityDeleteViewModel);
        }

        [HttpPost]
        public ActionResult Delete(Activity activity)
        {
            _service.Delete(activity);

            return RedirectToAction("Index");
        }*/
    }
}