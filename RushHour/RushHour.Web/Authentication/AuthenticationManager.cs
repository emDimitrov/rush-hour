﻿using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHour.Web.Authentication
{
    public static class AuthenticationManager
    {
        public static User LoggedUser
        {
            get
            {
                AuthenticationService service = null;
                UnitOfWork unitOfWork = new UnitOfWork();
                if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
                {
                    HttpContext.Current.Session["LoggedUser"] = new AuthenticationService(unitOfWork);
                }

                service = (AuthenticationService)HttpContext.Current.Session["LoggedUser"];

                return service.LoggedUser;
            }
        }

        public static void Authenticate(string username, string password)
        {
            AuthenticationService service = null;
            UnitOfWork unitOfWork = new UnitOfWork();

            if (HttpContext.Current != null && HttpContext.Current.Session["LoggedUser"] == null)
            {
                HttpContext.Current.Session["LoggedUser"] = new AuthenticationService(unitOfWork);
            }

            service = (AuthenticationService)HttpContext.Current.Session["LoggedUser"];
            service.AuthenticateUser(username, password);
        }

        public static void LogOut()
        {
            HttpContext.Current.Session["LoggedUser"] = null;
        }

    }
}