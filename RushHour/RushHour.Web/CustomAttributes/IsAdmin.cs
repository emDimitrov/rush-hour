﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RushHour.Web.CustomAttributes
{
    public class IsAdmin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var loggedUser = HttpContext.Current.Session["LoggedUser"];
            if (loggedUser.GetType().GetProperty("IsAdmin").GetValue(this,null).ToString() != "true")
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Users", action = "Login" }));
            }
        }
    }
}