﻿using AutoMapper;
using RushHour.Model;
using RushHour.Web.ViewModels;
using RushHour.Web.ViewModels.ActivityViewModel;
using RushHour.Web.ViewModels.AppointmentViewModel;
using RushHour.Web.ViewModels.UserViewModel;

namespace RushHour.Web.Mappings
{
    public class EntityMapper : Profile
    {
        public EntityMapper()
        {
            //ActivityMappers
            CreateMap<Activity, ActivityEditViewModel>();
            CreateMap<ActivityEditViewModel, Activity>();
            CreateMap<Activity, ActivityDeleteViewModel>();
            CreateMap<ActivityCreateViewModel, Activity>();
            CreateMap<ActivityBaseViewModel, Activity>();
            CreateMap<Activity, ActivityBaseViewModel>();
            CreateMap<ActivityIndexViewModel, Activity>();



            //AppointmentMappers
            CreateMap<Appointment, AppointmentEditViewModel>();
            CreateMap<ActivityCheckBox, AppointmentCreateViewModel>();

            //UserMappers
            CreateMap<User, UserIndexViewModel>();
            CreateMap<UserCreateViewModel, User>();
            CreateMap<User, UserEditViewModel>();
            CreateMap<UserEditViewModel, User>();
            CreateMap<User, UserDeleteViewModel>();
            CreateMap<UserBaseViewModel, User>();
            CreateMap<User, UserBaseViewModel>();

            //Base
            CreateMap<BaseModel, BaseViewModel>();
            CreateMap<BaseViewModel, BaseModel>();
        }
    }
}