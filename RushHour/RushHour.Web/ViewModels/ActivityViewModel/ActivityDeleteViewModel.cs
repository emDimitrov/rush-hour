﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RushHour.Web.ViewModels.ActivityViewModel
{
    public class ActivityDeleteViewModel : BaseViewModel
    {
        [DisplayName("Activity Name: ")]
        public string Name { get; set; }
    }
}