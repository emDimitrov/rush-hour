﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RushHour.Web.ViewModels.ActivityViewModel
{
    public class ActivityIndexViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Duration is required")]
        [Range(0, short.MaxValue, ErrorMessage = "Enter a positive value")]
        public short Duration { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid price")]
        public decimal Price { get; set; }
    }
}