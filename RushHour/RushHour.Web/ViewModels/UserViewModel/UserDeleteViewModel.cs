﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace RushHour.Web.ViewModels.UserViewModel
{
    public class UserDeleteViewModel : BaseDeleteViewModel
    {
        public string Email { get; set; }

        public string Name { get; set; }

    }
}