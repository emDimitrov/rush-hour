﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using RushHour.Model;
using System.Linq;
using System.Web;

namespace RushHour.Web.ViewModels.AppointmentViewModel
{
    public class AppointmentIndexViewModel
    {
        public int Id { get; set; }

        [DisplayName("Starting Time")]
        [Required(ErrorMessage = "The Starting Date and Time is required")]
        public string StartDateTime { get; set; }

        [DisplayName("Ending Time")]
        [Required(ErrorMessage = "The Ending Date and Time is required")]
        public string EndDateTime { get; set; }

        public int UserId { get; set; }

        public List<string> ActivityNames { get; set; }
    }
}