﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

namespace RushHour.Web.ViewModels.AppointmentViewModel
{
    public class ActivityCheckBox
    {
        public int Id { get; set; }

        public Model.Activity CheckedActivity { get; set; }

        public bool IsChecked { get; set; }
    }
}