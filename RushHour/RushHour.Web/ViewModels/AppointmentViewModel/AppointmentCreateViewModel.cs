﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RushHour.Web.ViewModels.AppointmentViewModel
{
    public class AppointmentCreateViewModel
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartDateTime { get; set; }

        public int UserId { get; set; }

        public List<ActivityCheckBox> Activities { get; set; }
    }
}