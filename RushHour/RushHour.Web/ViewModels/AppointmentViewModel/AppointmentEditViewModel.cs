﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RushHour.Web.ViewModels.AppointmentViewModel
{
    public class AppointmentEditViewModel
    {
        public int Id { get; set; }

        [DisplayName("Starting Time")]
        [Required(ErrorMessage = "The Starting Date and Time is required")]
        public DateTime StartDateTime { get; set; }

        [DisplayName("Ending Time")]
        public DateTime EndDateTime { get; set; }

        public int UserId { get; set; }

        public List<ActivityCheckBox> Activities { get; set; }
    }
}