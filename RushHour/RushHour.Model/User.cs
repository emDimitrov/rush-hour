﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Model
{
    [Table("Users")]
    public class User : BaseModel
    {
        [StringLength(254)]
        [Index(IsUnique = true)]
        [Required]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Phone { get; set; }

        [Required]
        public bool IsAdmin { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }
    }
}
