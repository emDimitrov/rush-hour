﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RushHour.Model
{
    [Table("Appointments")]
    public class Appointment : BaseModel
    {
        [DisplayName("Starting Time")]
        [Required(ErrorMessage = "The Starting Date and Time is required")]
        public DateTime StartDateTime { get; set; }

        [DisplayName("Ending Time")]
        [Required(ErrorMessage = "The Ending Date and Time is required")]
        public DateTime EndDateTime { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }

    }
}
