﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RushHour.Model
{
    [Table("Activities")]
    public class Activity : BaseModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public short Duration { get; set; }

        [Required]
        public decimal Price { get; set; }

        public virtual ICollection<Appointment> Appointment { get; set; }

    }
}
