﻿using AutoMapper;
using RushHour.API.MappingProfiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace RushHour.API.App_Start
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            cfg.AddProfiles(Assembly.GetExecutingAssembly()));
        }
    }
}