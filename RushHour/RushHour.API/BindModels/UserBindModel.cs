﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHour.API.BindModels
{
    public class UserBindModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public bool IsAdmin { get; set; }
    }
}