﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHour.API.BindModels
{
    public class ActivityBindModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public short Duration { get; set; }

        public decimal Price { get; set; }
    }
}