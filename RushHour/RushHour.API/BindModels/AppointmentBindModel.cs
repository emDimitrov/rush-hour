﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHour.API.BindModels
{
    public class AppointmentBindModel
    {
        public int Id { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public int UserId { get; set; }

        public IEnumerable<ActivityBindModel> Activities { get; set; }

    }
}