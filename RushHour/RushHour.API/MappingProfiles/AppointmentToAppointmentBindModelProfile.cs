﻿using AutoMapper;
using RushHour.API.BindModels;
using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RushHour.API.MappingProfiles
{
    public class AppointmentToAppointmentBindModelProfile : Profile
    {
        public AppointmentToAppointmentBindModelProfile()
        {
            CreateMap<Appointment, AppointmentBindModel>();
        }
    }
}