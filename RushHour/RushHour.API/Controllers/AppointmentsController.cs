﻿using AutoMapper;
using RushHour.API.BindModels;
using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RushHour.API.Controllers
{
    public class AppointmentsController : ApiController
    {

        private IService<Appointment> appointmentService;
        private IService<Activity> activityService;
        private IService<User> userService;

        private IUnitOfWork unitOfWork;

        public AppointmentsController()
        {
            unitOfWork = new UnitOfWork();
            appointmentService = new DataService<Appointment>(unitOfWork);
            activityService = new DataService<Activity>(unitOfWork);
            userService = new DataService<User>(unitOfWork);
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var appointments = appointmentService.GetAll()
                   .Select(t => new AppointmentBindModel
                   {
                       Id = t.Id,
                       UserId = t.UserId,
                       StartDateTime = t.StartDateTime,
                       EndDateTime = t.EndDateTime,
                       Activities = t.Activities.Select(p => new ActivityBindModel
                       {
                           Name = p.Name,
                           Duration = p.Duration,
                           Price = p.Price
                       })
                   }).ToList();

                return Ok(appointments);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var appointment = appointmentService.Get(id);
                AppointmentBindModel appointmentBindModel = new AppointmentBindModel
                {
                    Id = id,
                    UserId = appointment.UserId,
                    StartDateTime = appointment.StartDateTime,
                    EndDateTime = appointment.EndDateTime,
                    Activities = appointment.Activities.Select(p => new ActivityBindModel
                    {
                        Name = p.Name,
                        Duration = p.Duration,
                        Price = p.Price
                    })
                };

                return Ok(appointment);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpPost]
        public IHttpActionResult Post(AppointmentBindModel appointmentBindModel)
        {
            try
            {
                List<Activity> activities = new List<Activity>();
                foreach (var a in appointmentBindModel.Activities)
                {
                    Activity activity = activityService.Get(a.Id);
                    activities.Add(activity);
                }

                Appointment appointment = new Appointment
                {
                    UserId = appointmentBindModel.UserId,
                    StartDateTime = appointmentBindModel.StartDateTime,
                    EndDateTime = appointmentBindModel.EndDateTime,
                    Activities = activities
                };

                appointmentService.Insert(appointment);
                return Ok(appointment);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var appointment = appointmentService.Get(id);
                appointmentService.Delete(appointment);
                
                return Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpPut]
        public IHttpActionResult Put(int id, AppointmentBindModel appointmentBindModel)
        {
            Appointment appointment = appointmentService.Get(id);
            try
            {
                List<Activity> activities = new List<Activity>();
                foreach (var a in appointmentBindModel.Activities)
                {
                    Activity activity = activityService.Get(a.Id);
                    activities.Add(activity);
                }

                appointment.Id = id;
                appointment.UserId = appointmentBindModel.UserId;
                appointment.StartDateTime = appointmentBindModel.StartDateTime;
                appointment.EndDateTime = appointmentBindModel.EndDateTime;
                appointment.Activities = activities;

                appointmentService.Update(appointment);
                return Ok(appointment);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        //Get api/appointments?userId=
        /*[HttpGet]
        public IHttpActionResult GetUserAppointments(int userId)
        {
            Model.User user = userService.Get(userId);
            if (user != null)
            {
                return Ok(appointmentService.GetAll(x => x.UserId == userId).Select(m => Mapper.Map<Appointment, AppointmentBindModel>(m)));
            }
            else
            {
                return Content(HttpStatusCode.NotFound, "The user with id: " + userId.ToString() + " doesn't exist.");
            }
        }*/
    }
}
