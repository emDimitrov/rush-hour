﻿using AutoMapper;
using RushHour.API.BindModels;
using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RushHour.API.Controllers
{
    public class ActivitiesController : ApiController
    {
        private IService<Activity> activityService;
        private IUnitOfWork unitOfWork;

        public ActivitiesController()
        {
            unitOfWork = new UnitOfWork();
            activityService = new DataService<Activity>(unitOfWork);
        }

        //Get api/activities
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(activityService.GetAll().Select(m => Mapper.Map<Activity, ActivityBindModel>(m)));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        //Get api/activities/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            Activity activity = activityService.Get(id);
            if (activity == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<Activity, ActivityBindModel>(activity));
        }

        //POST api/activities
        [HttpPost]
        public IHttpActionResult Post(ActivityBindModel activityBindModel)
        {
            try
            {
                Activity activity = new Activity();
                Mapper.Map(activityBindModel, activity);
                activityService.Insert(activity);
                return Ok();
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, activityBindModel);
            }
        }

        //PUT api/activities/id
        [HttpPut]
        public IHttpActionResult Put(int id, ActivityBindModel activityBindModel)
        {
            Activity activity = activityService.Get(id);
            if (activity != null)
            {
                Mapper.Map(activityBindModel, activity);
                activityService.Update(activity);
                return Ok(activity);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Activity activity = activityService.Get(id);
            if (activity != null)
            {
                activityService.Delete(activity);
                return Ok(activity);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
