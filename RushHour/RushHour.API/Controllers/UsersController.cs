﻿using AutoMapper;
using RushHour.API.BindModels;
using RushHour.Data;
using RushHour.Model;
using RushHour.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RushHour.API.Controllers
{
    public class UsersController : ApiController
    {
        private IService<User> userService;
        private IUnitOfWork unitOfWork;

        public UsersController()
        {
            unitOfWork = new UnitOfWork();
            userService = new DataService<User>(unitOfWork);
        }

        //Get api/activities
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(userService.GetAll().Select(m => Mapper.Map<User, UserBindModel>(m)));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        //Get api/activities/id
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            User user = userService.Get(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<User, UserBindModel>(user));
        }

        //POST api/users
        [HttpPost]
        public IHttpActionResult Post(UserBindModel userBindModel)
        {
            try
            {
                User user = new User();
                Mapper.Map(userBindModel, user);

                userService.Insert(user);
                return Ok();
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.BadRequest, userBindModel);
            }
        }

        //PUT api/users/id
        [HttpPut]
        public IHttpActionResult Put(int id, UserBindModel userBindModel)
        {
            User user = userService.Get(id);
            
            if (user != null)
            {
                Mapper.Map(userBindModel, user);
                userService.Update(user);
                return Ok(user);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            User user = userService.Get(id);
            if (user != null)
            {
                userService.Delete(user);
                return Ok(user);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
