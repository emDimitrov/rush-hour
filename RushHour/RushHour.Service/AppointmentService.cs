﻿using RushHour.Data.Repositories;
using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.Data;

namespace RushHour.Service
{
    public class AppointmentService : DataService<Appointment>
    {
        public AppointmentService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public override void OnBeforeInsert(Appointment appointment)
        {
            for (int i = 0; i < appointment.Activities.Count; i++)
            {
                short duration = appointment.Activities.ToList()[i].Duration;
                appointment.EndDateTime = appointment.EndDateTime.AddMinutes(duration);
            }
        }

        public override void OnBeforeUpdate(Appointment appointment)
        {
            appointment.EndDateTime = appointment.StartDateTime;
            for (int i = 0; i < appointment.Activities.Count; i++)
            {
                short duration = appointment.Activities.ToList()[i].Duration;
                appointment.EndDateTime = appointment.EndDateTime.AddMinutes(duration);
            }
        }
    }
}
