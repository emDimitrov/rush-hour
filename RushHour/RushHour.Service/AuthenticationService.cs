﻿using RushHour.Data;
using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class AuthenticationService : DataService<User>
    {
        public AuthenticationService(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public User LoggedUser { get; set; }

        public void AuthenticateUser(string email, string password)
        {
            User user = Get(u => u.Email == email && u.Password == password);
            LoggedUser = user;
        }
    }
}

