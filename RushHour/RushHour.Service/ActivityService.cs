﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.Data.Repositories;
using RushHour.Data;

namespace RushHour.Service
{
    public class ActivityService : DataService<Activity>
    {
        public ActivityService(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
