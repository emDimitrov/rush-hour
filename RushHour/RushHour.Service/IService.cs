﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public interface IService<T> where T : BaseModel
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> filter);

        T Get(int id);
        T Get(Expression<Func<T, bool>> filter);

        void OnBeforeInsert(T entity);
        bool Insert(T entity);
        void OnAfterInsert(T entity);

        void OnBeforeUpdate(T entity);
        bool Update(T entity);
        void OnAfterUpdate(T entity);

        void OnBeforeDelete();
        bool Delete(T entity);
        void OnAfterDelete(T entity);

        bool IsValid(T entity);
    }
}
