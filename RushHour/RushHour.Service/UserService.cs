﻿using RushHour.Data;
using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class UserService : DataService<User>
    {
        private AppointmentService appointmentService;

        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            appointmentService = new AppointmentService(unitOfWork);
        }

        public override void OnAfterDelete(User user)
        {
            List<Appointment> appointments= appointmentService.GetAll(x => x.User == user).ToList();
            foreach (var appointment in appointments)
            {
                appointmentService.Delete(appointment);
            }
        }
    }
}
