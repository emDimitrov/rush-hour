﻿using RushHour.Data;
using RushHour.Data.Repositories;
using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class DataService<T> : IService<T> where T : BaseModel
    {
        private IUnitOfWork unitOfWork;
        public DataService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        #region Get
        public T Get(int id)
        {
            return unitOfWork.GetRepository<T>().Get(id);
        }

        public T Get(Expression<Func<T, bool>> filter)
        {
            return unitOfWork.GetRepository<T>().Get(filter);
        }
#endregion

        #region GetAll
        public IEnumerable<T> GetAll()
        {
            return unitOfWork.GetRepository<T>().GetAll().ToList();
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> filter)
        {
            return unitOfWork.GetRepository<T>().GetAll(filter).ToList();
        }
        #endregion

        #region Delete
        public void OnBeforeDelete()
        {
        }

        public bool Delete(T entity)
        {
            if (!IsValid(entity))
            {
                return false;
            }

            try
            {
                OnBeforeDelete();
                unitOfWork.GetRepository<T>().Delete(entity);
                unitOfWork.Commit();
                OnAfterDelete(entity);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public virtual void OnAfterDelete(T entity)
        {
        }
#endregion

        #region Insert
        public virtual void OnBeforeInsert(T entity)
        {
        }

        public bool Insert(T entity)
        {
            if (!IsValid(entity))
            {
                return false;
            }

            try
            {
                OnBeforeInsert(entity);
                unitOfWork.GetRepository<T>().Insert(entity);
                unitOfWork.Commit();
                OnAfterInsert(entity);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public virtual void OnAfterInsert(T entity)
        {
        }
#endregion

        #region Update
        public virtual void OnBeforeUpdate(T entity)
        {
        }

        public bool Update(T entity)
        {
            if (!IsValid(entity))
            {
                return false;
            }

            try
            {
                OnBeforeUpdate(entity);
                unitOfWork.GetRepository<T>().Update(entity);
                unitOfWork.Commit();
                OnAfterUpdate(entity);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public virtual void OnAfterUpdate(T entity)
        {
        }
        #endregion

        public virtual bool IsValid(T entity)
        {
            
            return true;
        }
    }
}
