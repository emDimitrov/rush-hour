﻿using RushHour.Data.Repositories;
using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        Repository<T> GetRepository<T>() where T : BaseModel;
 
    }
}
