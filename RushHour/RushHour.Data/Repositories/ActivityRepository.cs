﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class ActivityRepository : Repository<Activity>
    {
        private RushHourContext context;
        public ActivityRepository(RushHourContext context) : base(context)
        {
            this.context = context;
        }
    }
}
