﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class UserRepository : Repository<User>
    {
        private RushHourContext context;
        public UserRepository(RushHourContext context) : base(context)
        {
            this.context = context;
        }
    }
}
