﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data.Repositories
{
    public class AppointmentRepository : Repository<Appointment>
    {
        private RushHourContext context;
        public AppointmentRepository(RushHourContext context) : base(context)
        {
            this.context = context;
        }
    }
}
