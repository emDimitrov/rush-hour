﻿using RushHour.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Data
{
     public class RushHourContext : DbContext
    {
        public RushHourContext() : base("RushHourDb")
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<Appointment> Appointments { get; set; }

        public DbSet<Activity> Activities { get; set; }

    }
}
