namespace RushHour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDuration : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Activities", "Duration");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Activities", "Duration", c => c.DateTime(nullable: false));
        }
    }
}
