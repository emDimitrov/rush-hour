namespace RushHour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Duration = c.Double(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(nullable: false, maxLength: 254),
                        Password = c.String(nullable: false),
                        ConfirmPassword = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        IsAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true);
            
            CreateTable(
                "dbo.AppointmentsActivities",
                c => new
                    {
                        Appointments_Id = c.Int(nullable: false),
                        Activities_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Appointments_Id, t.Activities_Id })
                .ForeignKey("dbo.Appointments", t => t.Appointments_Id, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => t.Activities_Id, cascadeDelete: true)
                .Index(t => t.Appointments_Id)
                .Index(t => t.Activities_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Appointments", "UserId", "dbo.Users");
            DropForeignKey("dbo.AppointmentsActivities", "Activities_Id", "dbo.Activities");
            DropForeignKey("dbo.AppointmentsActivities", "Appointments_Id", "dbo.Appointments");
            DropIndex("dbo.AppointmentsActivities", new[] { "Activities_Id" });
            DropIndex("dbo.AppointmentsActivities", new[] { "Appointments_Id" });
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.Appointments", new[] { "UserId" });
            DropTable("dbo.AppointmentsActivities");
            DropTable("dbo.Users");
            DropTable("dbo.Appointments");
            DropTable("dbo.Activities");
        }
    }
}
