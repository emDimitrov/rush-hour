namespace RushHour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTableNames : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AppointmentsActivities", newName: "AppointmentActivities");
            RenameColumn(table: "dbo.AppointmentActivities", name: "Appointments_Id", newName: "Appointment_Id");
            RenameColumn(table: "dbo.AppointmentActivities", name: "Activities_Id", newName: "Activity_Id");
            RenameIndex(table: "dbo.AppointmentActivities", name: "IX_Appointments_Id", newName: "IX_Appointment_Id");
            RenameIndex(table: "dbo.AppointmentActivities", name: "IX_Activities_Id", newName: "IX_Activity_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AppointmentActivities", name: "IX_Activity_Id", newName: "IX_Activities_Id");
            RenameIndex(table: "dbo.AppointmentActivities", name: "IX_Appointment_Id", newName: "IX_Appointments_Id");
            RenameColumn(table: "dbo.AppointmentActivities", name: "Activity_Id", newName: "Activities_Id");
            RenameColumn(table: "dbo.AppointmentActivities", name: "Appointment_Id", newName: "Appointments_Id");
            RenameTable(name: "dbo.AppointmentActivities", newName: "AppointmentsActivities");
        }
    }
}
