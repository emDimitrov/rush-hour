namespace RushHour.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDuration2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activities", "Duration", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activities", "Duration");
        }
    }
}
