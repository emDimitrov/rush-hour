﻿using System;
using System.Collections.Generic;
using RushHour.Model;
using RushHour.Data.Repositories;

namespace RushHour.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RushHourContext context;
        private readonly IDictionary<Type, object> repositories = new Dictionary<Type, object>();

        public UnitOfWork()
        {
            this.context = new RushHourContext();
        }

        public Repository<T> GetRepository<T>() where T : BaseModel
        {
            Type typeOfModel = typeof(T);

            if (!this.repositories.ContainsKey(typeOfModel))
            {
                Type typeOfRepository = typeof(Repository<T>);
                this.repositories.Add(typeOfModel, Activator.CreateInstance(typeOfRepository, this.context));
            }

            return (Repository<T>)this.repositories[typeOfModel];
        }

        public void Commit()
        {
            this.context.SaveChanges();

        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.context != null)
                {
                    this.context.Dispose();
                }
            }
        }
    }
}
